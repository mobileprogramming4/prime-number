import 'dart:io';

import 'package:dart_application_3/dart_application_3.dart'
    as dart_application_3;

void main(List<String> arguments) {
  print('Enter number');
  var number = int.parse(stdin.readLineSync()!);
  if (isPrime(number)) {
    print('$number is a prime number.');
  } else {
    print('$number is not a prime number.');
  }
}

bool isPrime(int number) {
  for (var i = 2; i <= number / i; i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}
